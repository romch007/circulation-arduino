#include "Color.h"

#define RED 7
#define GREEN 8
#define BLUE 9
#define BUTTON 4
#define DELAY_TIME 1500

const Color* red_color = NULL;
const Color* green_color = NULL;
const Color* orange_color = NULL;

int button_state = 0;

void block() {
  set_color(orange_color);
  delay(DELAY_TIME);
  set_color(red_color);
}

void unblock() {
  set_color(green_color);
}

void set_color(Color* newColor) {
  analogWrite(RED, newColor->red);
  analogWrite(BLUE, newColor->blue);
  analogWrite(GREEN, newColor->green);
}

void handle_click() {
  block();
  delay(10000);
  unblock();
}

void setup() {
  Serial.begin(9600);

  red_color = new Color(255, 0, 0);
  green_color = new Color(0, 225, 0);
  orange_color = new Color(255, 165, 0);
  
  // put your setup code here, to run once:
  pinMode(RED, OUTPUT);
  pinMode(BLUE, OUTPUT);
  pinMode(GREEN, OUTPUT);
  pinMode(BUTTON, INPUT);

  set_color(green_color);
}

void loop() {
  // put your main code here, to run repeatedly:
  button_state = digitalRead(BUTTON);
  if (button_state == HIGH) {
    Serial.println("Pressed");
    handle_click();
    delay(1000);
  }
}
